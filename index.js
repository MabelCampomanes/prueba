const express = require('express');

const PORT = 3000;
const server = express();

const router = express.Router();

const movies = [
    {
        id: 1,
        name:'Harry Potter'
    },
    {
        id: 2,
        name:'Titanic'
    }
    {
        id: 3,
        name:  'Back to the Future'
    }
       ];

router.get('/', (req, res) => {
  res.send('Hello Upgrade Hub!');
});

router.get('/movies', (req, res) => {

    const { limit } = req.query;
    let result = movies;

    if(limit) {
        result = movies.slice(0, limit);
    }
  res.send(result);
});

router.get('/movies/:id', (req, res) => {

    const { id } = req.params; 
    //Guardo en una variable el valor introducido en la url
    const movie = movies.find(movie => movie.id == id);

    if(!movie){
        res.send('No se ha encontrado la película');
    }
    res.send(movie);
})

router.post('/movie', (req, res)=> {

    res.send('OK');

})

server.use('/', router);

server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});

